import { Injectable } from '@angular/core';
import moment from "moment";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class GerenteService {

  constructor() { }

  supervisorData = []

  pdf = null;

  downloadPdf (data) {
    
    this.supervisorData = this.clearData(data)

    const docDef: any = {
      content: [
        { text: moment().format("DD/MM/YYYY") + '\n\n',  fontSize: 12, alignment: 'right'},
        { text: 'INFORME DE GESTION DEALER', style: 'header'},
        {
          columns: [
            {
              width: '*',
              columns: [
                {
                  text: 'Total visitas: ',
                  bold: true
                },
                {
                  text: data.response.resumen.totalPoints
                }
              ],
            },
            {
              width: '*',
              columns: [
                {
                  text: 'Realizadas: ',
                  bold: true
                },
                {
                  text: data.response.resumen.totalSaleQuantity
                }
              ],
            },
            {
              width: '*',
              columns: [
                {
                  text: 'Ejecutado: ',
                  bold: true
                },
                {
                  text: (Math.round((data.response.resumen.totalSaleQuantity * 100 / data.response.resumen.totalPoints) * 10) / 10) + '%'
                }
              ],
            }
          ]
        },
        '\n\n',
      ],
      styles: {
        header: {
          fontSize: 25,
          bold: true,
          alignment: 'center',
          margin: [0, 10, 0, 20]
        },
        subheader: {
          fontSize: 15,
          bold: true,
          alignment: 'left',
          margin: [0, 20, 0, 5]
        },
        superMargin: {
          margin: [20, 0, 40, 0],
          fontSize: 15
        }
      }

    }


    this.supervisorData.forEach(supervisor => {

      let itemNameSupervisor = { text: supervisor.supervisorName + '\n\n', style: 'subheader' }
      let itemEstadistica =  {
        columns: [
          {
            width: '*',
            columns: [
              {
                text: 'Total visitas: ',
                bold: true
              },
              {
                text: supervisor.totalPoints
              }
            ],
          },
          {
            width: '*',
            columns: [
              {
                text: 'Realizadas: ',
                bold: true
              },
              {
                text: supervisor.totalSaleQuantity
              }
            ],
          },
          {
            width: '*',
            columns: [
              {
                text: 'Ejecutado: ',
                bold: true
              },
              {
                text: (Math.round((supervisor.totalSaleQuantity * 100 / supervisor.totalPoints) * 10) / 10) + '%'
              }
            ],
          }
        ]
      };

      let table = {
        layout: 'lightHorizontalLines', // optional
        table: {
          headerRows: 1,
          widths: [ '*', 'auto', 100, '*' ],
  
          body: [
            [ 'Asesor', 'Total', 'Realizadas', 'Ejecutado' ],
          ]
        }
      };

      supervisor.Asesores.forEach(asesor => {
        let itemAsesor = [asesor.AsesorName.split(' ').slice(0,2).join(' '), asesor.points, asesor.salesQuantity, Math.round((asesor.salesQuantity * 100 / asesor.points) * 10)/ 10 + '%']
        
        table.table['body'].push(itemAsesor)
      });

      docDef['content'].push(itemNameSupervisor)
      docDef['content'].push(itemEstadistica)
      docDef['content'].push('\n\n')
      docDef['content'].push(table)

    });


    this.pdf = pdfMake.createPdf(docDef);
    this.pdf.download('demo.pdf')
  }

  clearData (data) {

    let response = [];

    let { salesBySupervisorDealer } = data.response

    salesBySupervisorDealer.forEach(dataSupervisor => {
      let {name: supervisorName } = dataSupervisor.response.resumen.supervisor
      let {totalPoints, totalSaleQuantity } = dataSupervisor.response.resumen

      let auxDataSupervisor = {
        supervisorName,
        totalPoints,
        totalSaleQuantity,
        Asesores: []
      }
      let { sales } = dataSupervisor.response

      sales.forEach(sale => {
        let { points, salesQuantity } = sale.response
        let { name: AsesorName } = sale.response.user
        let auxAsesorData = {
          AsesorName,
          points,
          salesQuantity
        } 
        auxDataSupervisor.Asesores.push(auxAsesorData)
      });
      response.push(auxDataSupervisor)
    });

    return response;
     
  }

}
