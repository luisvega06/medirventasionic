import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import axios from 'axios';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  

  constructor( private http: HttpClient ) { }

  userToken: string;
  urlBase: string = 'https://gomovil.net/api/'

  // get(url: string){
  //   return axios.get(url);
  // }

  get(url: string) {
    return this.http.get(url)
    .pipe(
      map( resp => {
        return resp;
      })
    );
  }

  post( url: string, datos: object ) {
    return this.http.post( url, datos )
    .pipe(
      map( resp => {
        return resp;
      })
    )
  }

  

 
}
