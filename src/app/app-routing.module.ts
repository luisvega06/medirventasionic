import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registrar',
    loadChildren: () => import('./registrar/registrar.module').then( m => m.RegistrarPageModule)
  },
  {
    path: 'buscar',
    loadChildren: () => import('./buscar/buscar.module').then( m => m.BuscarPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:'*'
    }
  },
  {
    path: 'punto',
    loadChildren: () => import('./punto/punto.module').then( m => m.PuntoPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:'*'
    }
  },
  {
    path: 'venta',
    loadChildren: () => import('./venta/venta.module').then( m => m.VentaPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:['asesor pda', 'asesor pdv', 'asesor mixto']
    }
  },
  {
    path: 'supervisor',
    loadChildren: () => import('./supervisor/supervisor.module').then( m => m.SupervisorPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:['supervisor dealer']
    }
  },
  {
    path: 'asesor',
    loadChildren: () => import('./asesor/asesor.module').then( m => m.AsesorPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:'*'
    }
  },
  {
    path: 'gerente',
    loadChildren: () => import('./gerente/gerente.module').then( m => m.GerentePageModule),
    canActivate:[AuthGuard],
    data:{
      rol:['supervisor colombia movil', 'ejecutivo', 'desarrolador', 'gerente dealer', 'gerente']
    }

  },
  {
    path: 'supervisor-view',
    loadChildren: () => import('./supervisor-view/supervisor-view.module').then( m => m.SupervisorViewPageModule),
    canActivate:[AuthGuard],
    data:{
      rol:'*'
    }
  },
  {
    path: 'pendientes',
    loadChildren: () => import('./pendientes/pendientes.module').then( m => m.PendientesPageModule)
  },
  {
    path: 'mapa',
    loadChildren: () => import('./mapa/mapa.module').then( m => m.MapaPageModule)
  },
  {
    path: 'master',
    loadChildren: () => import('./master/master.module').then( m => m.MasterPageModule)
  },
  {
    path: 'visits',
    loadChildren: () => import('./visits/visits.module').then( m => m.VisitsPageModule)
  },  {
    path: 'register-line',
    loadChildren: () => import('./developers/register-line/register-line.module').then( m => m.RegisterLinePageModule)
  },

  

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
