import { Component, OnInit } from '@angular/core';
import { JwtService } from '../service/jwt.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.page.html',
  styleUrls: ['./master.page.scss'],
})
export class MasterPage implements OnInit {

  user:any = {};

  constructor( private jwtService: JwtService ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getToken();
    
  }

  getToken() {
    const token = localStorage.getItem('token');
    this.user = this.jwtService.parseJwt(token);
    console.log('datos del usuario que ingresa ',this.user.id);
    
      
  }

}
