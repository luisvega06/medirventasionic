import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpService } from '../service/http.service';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as Mapboxgl from 'mapbox-gl';
declare var google;

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}





@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {
  private url = this.service.urlBase + 'dealer/gps';
  user = {
    user_id: 47
  }

  gpsCenter: any = {
    lat: 0,
    lng: 0
  };

  markers: Marker[] = [];
  lat:number;
  lgn:number;
  //mapa: Mapboxgl.Map;
  map = null;
  validation = {
    mostrar: 0
  }

  constructor( public geolocation:Geolocation,
               private service: HttpService,
               public navCtrl: NavController,
               private route: ActivatedRoute ) { }


  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getIdUser();
    this.getGeolocation();
    this.getZone();
    
  }

  getIdUser() {
    this.route.queryParams.subscribe( params => {
      try{
        this.user.user_id = JSON.parse(params['id']);
        console.log('id del usuario a consultar',this.user);
      }catch(ex){
        console.log(ex);
      }
    });
  }

  getZone() {
    
    this.service.post(this.url, this.user )
      .subscribe( resp => {
        console.log('nueva api', resp);
        let data = resp['data'];
        console.log( 'data', data);
        if( data.length == 0 ){
          console.log('no hay datos para mostrar');
          this.validation.mostrar = 1;
        }else{
          this.validation.mostrar = 0;
          data.forEach(marker => {
            console.log('maerker: ', marker['position'].lat);
            marker['position'].lat = parseFloat(marker['position'].lat);
            marker['position'].lng = parseFloat(marker['position'].lng);
            this.markers.push(marker)
          });
          console.log('marker data response: ', this.markers);
          this.markers = resp['data'];
          let x = this.markers.length - 1;
          console.log('ultima position array: ', x);
          
          this.gpsCenter.lat = parseFloat(resp['data'][x]['position']['lat']);
          this.gpsCenter.lng = parseFloat(resp['data'][x]['position']['lng']);
          console.log('render', this.markers);
          console.log('center', this.gpsCenter.lng);
          this.loadMap();
        }
        
        
      }, (err) => {
        console.log('error', err);
      })
  }

  getGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('latitud= ', resp.coords.latitude);
      console.log('longitud= ', resp.coords.longitude);
      this.lat = resp.coords.latitude;
      this.lgn = resp.coords.longitude;
      //this.mapMapbox();
      
      // resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });

     
  }

  /*mapMapbox() {
    Mapboxgl.accessToken = environment.mapboxkey;
    this.mapa = new Mapboxgl.Map({
    container: 'mapa-mapbox', // container id
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [this.lgn, this.lat], // starting position
    zoom: 16 // starting zoom
    });
  }*/

  loadMap() {
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('mapa-mapbox');
    // create LatLng object
    
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.gpsCenter,
      zoom: 16
    });
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      this.renderMarkers();
      mapEle.classList.add('show-map');
    });
  }

  renderMarkers() {
    this.markers.forEach((marker,index) => {
      this.addMarker(marker,index+1);
    });
  }

  addMarker(marker: Marker, a: Number) {
    let marker_custom = new google.maps.Marker({
      title: marker.title,
      position: marker.position,
      map: this.map,
      icon: {  url: "../../assets/images/marcador.png",
               scaledSize: new google.maps.Size(30, 30)
      }
      
    });
    let content = a+marker.title;
    this.addInfoWindow(marker_custom, content);
    
  }

  addInfoWindow(marker: Marker, content) {

    let infoWindow = new google.maps.InfoWindow({
        content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
    });

}

}
