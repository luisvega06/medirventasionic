export class LineModel {

  point_code: string;
  line: string;
  line_port: string;
  type: string;
  operator: string;
  user_id: string;
  tactico: string;
  portability: string;

}