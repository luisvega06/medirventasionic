import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SupervisorViewPageRoutingModule } from './supervisor-view-routing.module';

import { SupervisorViewPage } from './supervisor-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SupervisorViewPageRoutingModule
  ],
  declarations: [SupervisorViewPage]
})
export class SupervisorViewPageModule {}
