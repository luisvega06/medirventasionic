import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupervisorViewPage } from './supervisor-view.page';

const routes: Routes = [
  {
    path: '',
    component: SupervisorViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupervisorViewPageRoutingModule {}
