import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../service/http.service';
import { NavController } from '@ionic/angular';
import { JwtService } from '../service/jwt.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-supervisor-view',
  templateUrl: './supervisor-view.page.html',
  styleUrls: ['./supervisor-view.page.scss'],
})
export class SupervisorViewPage implements OnInit {

  idSuper: number;
  private url = this.service.urlBase + 'dealer/ventas/resumen/';
  asesores: any = [];
  user:any = [];
  resul:any = [];

  constructor( private route: ActivatedRoute,
               private service: HttpService,
               public navCtrl: NavController,
               private jwtService: JwtService ) { }

  ngOnInit() {
    this.supervisor();
    this.getToken();
  }

  getToken() {
    const n = localStorage.getItem('token');
    if(n != ''){
      const token = localStorage.getItem('token');
      this.user = this.jwtService.parseJwt(token);
      
      console.log(this.user);
      
    }
    
  }

  supervisor() {
    this.route.queryParams.subscribe( params =>{
      try{
        this.idSuper = JSON.parse(params['idSuper']);
        console.log(this.idSuper);
        this.getAsesores();
      }catch(ex){
        console.log(ex);

      }
    })
  }

  viewAsesor(id: number) {
    console.log('id asesor', id);
    let NavigationExtras: NavigationExtras = {
      queryParams: {
        idAsesor: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['/asesor'], NavigationExtras);
  }


  getAsesores() {
    this.service.get(`${this.url}${this.idSuper}`)
      .subscribe( resp => {
        console.log('es este',resp);
        this.asesores = resp['response']['sales'];
        this.resul = resp;
        //console.log('esto', this.resul.response.resumen.totalSaleQuantity);
        //console.log('asesores', this.asesores);
      }, (err) => {
        console.log(err);
      })
  }

}
