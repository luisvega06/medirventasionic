import { Component } from '@angular/core';
//import { timer } from 'rxjs/Observable/timer';
import { Platform } from '@ionic/angular';
//import { Plugins } from '@capacitor/core';
//const { SplashScreen } = Plugins;
import { timer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash = true;
  constructor(
    private platform: Platform
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //SplashScreen.hide();
      timer(3000).subscribe(() => this.showSplash = false);
    });
  }
}
