import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import Swal from 'sweetalert2';
import swal from 'sweetalert';



@Component({
  selector: 'app-punto',
  templateUrl: './punto.page.html',
  styleUrls: ['./punto.page.scss'],
})
export class PuntoPage implements OnInit {
  //attention_to_the_public: string = null;
  resul:any = [];
  private url = this.service.urlBase + 'dealer/visita/crear';
  option: string;
  user: any = {};
  lat:number;
  lon:number;
  total:string
  tigoP = false;
  tigoN = false;
  claroP = false;
  claroN = false;
  movistarP = false;
  movistarN = false;
  wowP = false;
  wowN = false;
  saleChhipN = false;
  saleChipP = false;
  saldoR = false;
  montoR = false;
  options = {
    chip: 0,
    recarga: 0
  }

  Filtro = {
    atencionP: 0,
    chip: 0,
    recarga: 0
  }

  validaciones = {
    chip:      0,
    chipI:     0,
    chipV:     0,
    recarga:   0,
    noVenta:   0,
    cerrado:   0,
    abierto:   0,
    domicilio: 0
  }

  sale = {
    point_sale_id: null,
    user_id: null,
    simcard_inventary_quantity: null,
    simcard_sale: null,
    simcard_sale_quantity: null,
    simcard_reasons_not_sale:null,
    simcard_500mb_inventary_quantity:null,
    simcard_500mb_sale:null,
    simcard_500mb_sale_quantity:null,
    simcard_500mb_reasons_not_sale:null,
    recharge_inventary_quantity:null,
    recharge_sale:null,
    recharge_sale_quantity:null,
    visited:null,
    reasons_not_visited:null,
    attention_to_the_public:null,
    observations_sale:null,
    distance_adviser_point:null,
    simcard_inventary_quantity_movistar: null,
    simcard_inventary_quantity_claro: null,
    simcard_inventary_quantity_wom: null,
    simcard_inventary_quantity_virgin: null,
    simcard_pre_recharge_inventary_quantity_movistar: null,
    simcard_pre_recharge_inventary_quantity_claro: null,
    simcard_pre_recharge_inventary_quantity_wom: null,
    simcard_pre_recharge_inventary_quantity_virgin: null,
    user_lat: null,
    user_lon: null
  }
  constructor( private route: ActivatedRoute,
               public navCtrl: NavController,
               private service: HttpService,
               private jwtService: JwtService,
               public geolocation:Geolocation ) { }

  punto() {

    this.route.queryParams.subscribe( params => {
      try{
        this.resul = JSON.parse(params['pdv']);
        console.log('holaa',this.resul);
        this.ionViewWillEnter();
      }catch(ex){
        console.log(ex);
      }
    });

  }
  ionViewWillEnter() {
    this.getGeolocation();
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.ionViewWillEnter();
    setTimeout(() => {
      console.log('Async operation has ended');

      event.target.complete();
    }, 3000);
  }

  getGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('latitud= ', resp.coords.latitude);
      console.log('longitud= ', resp.coords.longitude);
      this.lat = resp.coords.latitude;
      this.lon = resp.coords.longitude;
      this.sale.user_lat = resp.coords.latitude;
      this.sale.user_lon = resp.coords.longitude;
      // resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
       this.alertV('No se cuentan con los permisos, por favor encienda el GPS y vuelva a intentar');
     });

     
  }

  choose(data: string) {
    console.log('data de ion segment', data);
    if(data == 'I'){
      this.validaciones.chipI = 1;
      this.validaciones.chipV = 0;
    }else{
      if(data == 'V'){
        this.validaciones.chipI = 0;
        this.validaciones.chipV = 1;
      }
    }
  }

  getToken() {
    const n = localStorage.getItem('token');
    if(n != ''){
      const token = localStorage.getItem('token');
      this.user = this.jwtService.parseJwt(token);
      this.sale.user_id = this.user.id;
      this.sale.point_sale_id = this.resul.response.id;
      console.log(this.user);
      console.log(this.sale);
    }
    
  }

  atencionPublico( dato ) {
    if(dato.detail.value == 'a' ) {
      this.Filtro.atencionP = 1;
      this.validaciones.abierto = 1;
      this.sale.attention_to_the_public = 1;
    }else{
      if(dato.detail.value == 'd') {
        this.Filtro.atencionP = 1;
        this.validaciones.abierto = 1;
        this.validaciones.domicilio = 1;
        this.sale.attention_to_the_public = 2;

      }else{
        if(dato.detail.value == 'c') {
          this.Filtro.atencionP = 1;
          this.validaciones.abierto = 0;
          this.validaciones.domicilio = 0;
          this.sale.attention_to_the_public = 0;
          //aca cancelamos todos los input y habilitamos las observaciones del punto cerrado
          this.validaciones.chip = 0;
          this.validaciones.recarga = 0;
          this.validaciones.noVenta = 0;
          this.options.chip = 0;

        }else {
          if(dato.detail.value == 't') {
            this.Filtro.atencionP = 1;
            this.validaciones.abierto = 0;
            this.validaciones.domicilio = 0;
            this.sale.attention_to_the_public = 3;
            //aca cancelamos todos los input y habilitamos las observaciones del punto cerrado
            this.validaciones.chip = 0;
            this.validaciones.recarga = 0;
            this.validaciones.noVenta = 0;
            this.options.chip = 0;
  
          }
        }
      }
    }
    console.log(this.sale);
  }

  acciones( dato: string ) {
     console.log( dato );
    this.option = dato;
    if(dato == 'c') {
      this.Filtro.chip = 1;
      this.Filtro.recarga = 0;
      this.validaciones.chip = 1;
      this.options.chip = 1;
      this.validaciones.recarga = 0;
      this.validaciones.noVenta = 0;
    }else{
      if(dato == 'r') {
        this.Filtro.recarga = 1;
        this.Filtro.chip = 0;
        this.validaciones.chip = 0;
        this.options.chip = 0;
        this.validaciones.recarga = 1;
        this.validaciones.noVenta = 0;
      }else{
        if(dato == 'cr') {
          this.validaciones.chip = 1;
          this.options.chip = 1;
          this.validaciones.recarga = 1;
          this.validaciones.noVenta = 0;
        }else{
          if(dato == 'n') {
            this.validaciones.chip = 0;
            this.options.chip = 0;
            this.validaciones.recarga = 0;
            this.validaciones.noVenta = 1;
            this.Filtro.chip = 0;
            this.Filtro.recarga = 0;
          }
        }
      }
    }
  }

  ngOnInit() {
    this.punto();
    this.getToken();
    this.getGeolocation();
  }

  alertV( msj ) {
    swal({
      title: "Upps'",
      text: msj,
      icon: "error",
      
    });
  }

  validateSave() {
    if(this.Filtro.atencionP == 0){
      this.alertV('Eliga una opción para seguir con la visita al punto de venta');
    }else{
      if(this.Filtro.chip == 1 && this.sale.simcard_inventary_quantity == null){
        this.alertV('Campo chip normal tigo es necesario');
        this.tigoN = true;
        this.tigoP = false;
        this.claroP = false;
        this.claroN = false;
        this.movistarP = false;
        this.movistarN = false;
      }else{
        if(this.Filtro.chip == 1 && this.sale.simcard_500mb_inventary_quantity == null){
          this.alertV('Campo inventario chip precargado tigo es necesario');
          this.tigoP = true;
          this.tigoN = false;
          this.claroP = false;
          this.claroN = false;
          this.movistarP = false;
          this.movistarN = false;
        }else{
          if(this.Filtro.chip == 1 && this.sale.simcard_sale_quantity == null){
            this.alertV('Campo venta chip normal tigo es necesario / pestaña venta');
            this.saleChhipN = true;
            this.saleChipP = true;
          }else{
            if(this.Filtro.chip == 1 && this.sale.simcard_500mb_sale_quantity == null){
              this.alertV('Campo venta chip precargado tigo es necesario / pestaña venta');
              this.saleChipP = true;
              this.saleChhipN = false;
            }else{
              if(this.Filtro.recarga == 1 && this.sale.recharge_inventary_quantity == null){
                this.alertV('El campo de inventario de recarga es necesario');
                this.saldoR = true;
                this.montoR = false;
              }else{
                if(this.Filtro.recarga == 1 && this.sale.recharge_sale_quantity == null){
                  this.alertV('El Campo monto es ncesario');
                  this.montoR = true;
                  this.saldoR = false;
                }else{
                  if(this.Filtro.chip ==1 && this.sale.simcard_inventary_quantity_claro == null){
                    this.alertV('Campo inventario chip normal claro es necesario');
                    this.claroN = true;
                    this.tigoP = false;
                    this.tigoN = false;
                    this.claroP = false;
                    this.movistarP = false;
                    this.movistarN = false;
                  }else{
                    if(this.Filtro.chip == 1 && this.sale.simcard_pre_recharge_inventary_quantity_claro == null){
                      this.alertV('Campo inventario chip precargado claro es necesario');
                      this.claroP = true;
                      this.tigoP = false;
                      this.tigoN = false;
                      this.claroN = false;
                      this.movistarP = false;
                      this.movistarN = false;
                    }else{
                      if(this.Filtro.chip == 1 && this.sale.simcard_inventary_quantity_movistar == null){
                        this.alertV('Campo inventario chip normal movistar es necesario');
                        this.movistarN = true;
                        this.tigoP = false;
                        this.tigoN = false;
                        this.claroP = false;
                        this.claroN = false;
                        this.movistarP = false;
                      }else{
                        if(this.Filtro.chip == 1 && this.sale.simcard_pre_recharge_inventary_quantity_movistar == null){
                          this.alertV('Campo inventario chip precargado movistar es necesario');
                          this.movistarP = true;
                          this.tigoP = false;
                          this.tigoN = false;
                          this.claroP = false;
                          this.claroN = false;
                          this.movistarN = false;
                        }else{
                          this.sale.user_lat = this.lat;
                          this.sale.user_lon = this.lon;
                          this.tigoN = false;
                          this.tigoP = false;
                          this.claroP = false;
                          this.claroN = false;
                          this.movistarP = false;
                          this.movistarN = false;
                          

                          if(this.sale.user_lat == null && this.sale.user_lon == null){
                            this.alertV('Por favor encienda el GPS o de click en obtener ubicación');
                            //this.getGeolocation();
                          }else{
                            console.log('antes de enviar:  ', this.sale);
                             this.saveSale();
                          }
                         
                        }
                      }
                    }
                  }
                  
                }
              }
            }
          }
        }
      }
    }
  }

  saveSale() {

    Swal.fire({
      icon: 'info',
      allowOutsideClick:false,
      text: 'Espere por Favor',
      
    });

    Swal.showLoading();

    // console.log(this.sale);
    this.service.post(`${this.url}`, this.sale)
      .subscribe( resp => {
        console.log('venta realizada',resp);
        Swal.close();
        // inicio del alerte 
        Swal.fire({
          title: 'Venta guardada con exito',
          text: "seguir buscando PDV/PDA!",
          icon: 'success',
          allowOutsideClick:false,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continuar'
        }).then((result) => {
          if (result.isConfirmed) {
            this.navCtrl.navigateForward(['/buscar']);
          }
        })
        // fin del alert
      }, ( err ) => {
        console.log(err);
        Swal.close();
      } )
  }

}
