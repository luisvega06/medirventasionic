import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.page.html',
  styleUrls: ['./pendientes.page.scss'],
})
export class PendientesPage implements OnInit {
  private url = this.service.urlBase + 'dealer/ventas/';
  private urlPunto = this.service.urlBase + 'dealer/visita/';
  user:any =[];
  points:any = [];
  respuesta:any = [];

  constructor( private service: HttpService,
               private jwtService: JwtService,
               public navCtrl: NavController ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getToken();
  }

  getToken() {
    const token = localStorage.getItem('token');
    if(!token){
      console.log('no se tiene ninguna autenticación');
    }else{
      this.user = this.jwtService.parseJwt(token);
      this.getPoints();
    }
  }

  getPoints() {
    this.service.get(`${this.url}${this.user.id}`)
      .subscribe( resp  => {
        console.log(resp);
        this.points = resp['response']['visitsFinal'];
        console.log('puntos que faltan ll',this.points);
      }, (err) =>{
        console.log(err);
      })
  }

  visitPoint(id: number) {
    console.log('id punto es:', id);
    Swal.fire({
      icon: 'info',
      allowOutsideClick:false,
      text: 'espere por favor',
      
    });
    Swal.showLoading();

    this.service.get(`${this.urlPunto}${id}`)
      .subscribe( resp => {
        this.respuesta = resp;
        //console.log(this.respuesta);
         Swal.close();
        let NavigationExtras: NavigationExtras = {
          queryParams: {
            pdv: JSON.stringify(this.respuesta)
          }
        };
        this.navCtrl.navigateForward(['/punto'], NavigationExtras);
      },(err) => {
        console.log(err);
      })
  }

}
