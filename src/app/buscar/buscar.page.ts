import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import swal from 'sweetalert';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {

  credential = {
    id: undefined
  }
  user: any = {};

  private url =  this.service.urlBase +'dealer/visita/';

  resul:any = [];

  constructor( private service: HttpService,
               public navCtrl: NavController,
               private jwtService: JwtService, ) { }

  ngOnInit() {
    
    //this.getToken();
  }

  salir() {
    localStorage.removeItem('token');
    this.navCtrl.navigateForward(['/login']);
  }

  ionViewWillEnter() {
    this.getToken();
    this.credential.id = '';
  }

  getToken() {
    const token = localStorage.getItem('token');
    if(!token){
      this.navCtrl.navigateForward(['/login']);
    }else{
      this.user = this.jwtService.parseJwt(token);
      console.log(this.user.rol);
    }
  }

  // async buscarPunto() {
  //   const data = this.service.get("https://back.teloconsigo.net/api/dealer/visita/677587")
  //                 .then( data => {
  //                   console.log(data);
  //                 })
  //                 .catch(error => {
  //                   console.log(error.response);
                    
  //                   //this.presentAlert(error.response.data.data)
  //                 });
  // }

  buscar() {

    console.log('buscando...');
    if (this.credential.id > 999 || undefined) {
      Swal.fire({
        icon: 'info',
        allowOutsideClick:false,
        text: 'espere por favor',
        
      });

      Swal.showLoading();

      
      this.service.get(`${this.url}${this.credential.id}`)
      .subscribe( resp =>  {
        this.resul = resp;
        if (this.resul.response.name == undefined) {
          Swal.close();
          swal({
            title: this.credential.id + " PDA/PDV no existe!",
            text: "Por favor verificar que el id este correcto",
            icon: "error",
            
          });
        }else{
          Swal.close();
          let NavigationExtras: NavigationExtras = {
            queryParams: {
              pdv: JSON.stringify(this.resul)
            }
          };
          this.navCtrl.navigateForward(['/punto'], NavigationExtras);
        }
      }, ( err ) => {
        console.log(err);
      })
    }else{
      swal(" Por favor ingresa un valor real a buscar!");
    }
    

  }

}
