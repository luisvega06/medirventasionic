import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { GerenteService } from '../service/report/gerente.service'



@Component({
  selector: 'app-gerente',
  templateUrl: './gerente.page.html',
  styleUrls: ['./gerente.page.scss'],
})
export class GerentePage implements OnInit {

  private url = this.service.urlBase + 'dealer/ventas/resumen';
  supervisores: any = [];
  supervisores2: any = [];
  user: any = {};

  data = null

  dato = {
    user_id: null
  }


  constructor(private service: HttpService,
    private jwtService: JwtService,
    public navCtrl: NavController,
    public gerenteService: GerenteService
    ) { }

  ngOnInit() {
   
  }

  color(supervisor) {
    let rango = supervisor.response.resumen.totalSaleQuantity / supervisor.response.resumen.totalPoints * 100
    let itemColor = '';
    if (rango < 31) {
      itemColor = 'danger';
    } else if (rango > 30 && rango < 70) {
      itemColor = 'warning';
    } else if (rango > 70) {
      itemColor = 'secondary';
    }
    return itemColor;
  }

  downloadPdf (data) {
    this.gerenteService.downloadPdf(data)
  }

  salir() {
    localStorage.removeItem('token');
    this.navCtrl.navigateForward(['/login']);
  }

  ionViewWillEnter() {
    this.getToken();
    this.getSupervisores();
  }

  getToken() {
    const token = localStorage.getItem('token');
    this.user = this.jwtService.parseJwt(token);
    this.dato.user_id = this.user.id;


  }

  viewSupervisor(id: number) {
    //console.log(id);
    let NavigationExtras: NavigationExtras = {
      queryParams: {
        idSuper: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['supervisor-view'], NavigationExtras);
  }

  async getSupervisores() {

    await this.service.post(this.url, { user_id: this.user.id })
      .subscribe(resp => {
        this.data = resp;
        let supervisors = resp["response"]["supervisorsDealers"];

        let resul = resp['response']['salesBySupervisorDealer'];
        resul.forEach((element, index) => {
          this.supervisores[index] = element;
        });
        supervisors.forEach((element2, index2) => {
          this.supervisores2[index2] = element2;
        });

        /* 
                for(let i= 0; i < resul.length; i++){
                  console.log(resul[i]['response'] );
                   this.supervisores.push(resul[i]['response'] );
                   
                } */
      }, (err) => {
        console.log(err);
      })


  }

}
