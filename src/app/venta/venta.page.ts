import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.page.html',
  styleUrls: ['./venta.page.scss'],
})
export class VentaPage {
  
  user: any = {};
  private url = this.service.urlBase + 'dealer/ventas/';
  private urlPunto = this.service.urlBase + 'dealer/visita/';
  
  respuesta:any =[];
  resul:any = [];
  points:any =[];

  validacion = {
    puntos: 0
  }

  constructor( private service: HttpService,
               private jwtService: JwtService,
               public navCtrl: NavController ) { }

  // ngOnInit() {
  //   this.getToken();
    
  // }

  salir() {
    localStorage.removeItem('token');
    this.navCtrl.navigateForward(['/login']);
  }

  visitPoint(id: number) {
    console.log('id punto es:', id);
    Swal.fire({
      icon: 'info',
      allowOutsideClick:false,
      text: 'espere por favor',
      
    });
    Swal.showLoading();

    this.service.get(`${this.urlPunto}${id}`)
      .subscribe( resp => {
        this.respuesta = resp;
        //console.log(this.respuesta);
         Swal.close();
        let NavigationExtras: NavigationExtras = {
          queryParams: {
            pdv: JSON.stringify(this.respuesta)
          }
        };
        this.navCtrl.navigateForward(['/punto'], NavigationExtras);
      },(err) => {
        console.log(err);
      })


  }
  
  ionViewWillEnter() {
    this.getToken();
  }

  validar( dato ) {
    if(dato == 'n')
      this.validacion.puntos = 0

    if(dato == 'a')
      this.validacion.puntos = 1
  }

  getToken() {
    const token = localStorage.getItem('token');
    if(!token){
      this.navCtrl.navigateForward(['/login']);
    }else{
      this.user = this.jwtService.parseJwt(token);
      if(this.user.rol != 'asesor pda'){
        if(this.user.rol != 'asesor pdv'){
          if(this.user.rol != 'asesor mixto'){
            if(this.user.rol == 'supervisor dealer'){
              this.navCtrl.navigateForward(['/supervisor']);
              console.log('me voy pa supervisor dealer');
            }else{
              this.navCtrl.navigateForward(['/gerente']);
              console.log('me voy pa gerente');
            }
          }
          
        }
        
      }
      this.misVisitas();
    }
    
  }
  
  misVisitas() {
    
    this.service.get(`${this.url}${this.user.id}`)
      .subscribe( resp =>  {
        this.resul = resp;
        this.points = resp['response']['visitsFinal'];
        //console.log('puntos ll', this.points);
        console.log( this.resul );
      }, (err ) => {
        console.log(err);
      })

  }

}
