import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from '../models/usuario.model';
import { NgForm } from '@angular/forms';
import { HttpService } from '../service/http.service';
import Swal from 'sweetalert2';
import swal from 'sweetalert';
import { NavController } from '@ionic/angular';
import { JwtService } from '../service/jwt.service';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: UsuarioModel;
  private url = this.service.urlBase + 'login';
  user: any = {};

  resul:any = [];

  constructor( private service: HttpService,
               public navCtrl: NavController,
               private jwtService: JwtService ) { }

  ngOnInit() {
    this.usuario = new UsuarioModel();
    console.log(this.usuario);
    
    this.getToken();
  }
  ionViewWillEnter() {
    SplashScreen.hide();
    this.getToken();
    
  }

  getToken() {
    const token = localStorage.getItem('token');
    if(token){
      this.user = this.jwtService.parseJwt(token);
      console.log(this.user);
      if(this.user.rol == 'desarrollador'){
        this.navCtrl.navigateForward(['/register-line']);
        console.log('aqui');
        
      }
      if(this.user.rol == 'asesor pda' || 'asesor pvd' || 'asesor mixto'){
        this.navCtrl.navigateForward(['/venta']);
      }else{
        if(this.user.rol == 'supervisor dealer'){
          this.navCtrl.navigateForward(['/supervisor']);

        }else{
          this.navCtrl.navigateForward(['/gerente']);
        }
      }
    }
    
  }

  login (form: NgForm) {
    if( form.invalid ) { return; }
    console.log('formulario valido');
    console.log(this.usuario);
    Swal.fire({
      icon: 'info',
      allowOutsideClick:false,
      text: 'espere por favor',
      
    });

    Swal.showLoading();
    this.service.post(`${this.url}`, this.usuario)
      .subscribe( resp => {
        this.resul = resp;
        console.log(this.resul.token);
        localStorage.setItem('token', this.resul.token);
        Swal.close();

        this.getToken();
        
      }, ( err ) => {
        //console.log(err);
        if (err.data == 'El usuario no existe' || 'Credenciales incorrectas') {
          swal({
            title:" Credenciales incorrectas",
            text: "Por favor verificar y vulva a intentar",
            icon: "error",
            
          });
        }
        Swal.close();
      })

  }


}
