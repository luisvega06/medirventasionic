import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { JwtService } from '../service/jwt.service';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.page.html',
  styleUrls: ['./supervisor.page.scss'],
})
export class SupervisorPage implements OnInit {

  private url = this.service.urlBase + 'dealer/ventas/resumen/';
  user: any = {};
  resul:any = [];
  asesores:any = [];



  constructor( private service: HttpService,
               private jwtService: JwtService,
               public navCtrl: NavController ) { }

  ngOnInit() {
    // this.getToken();
    
  }

  salir() {
    localStorage.removeItem('token');
    this.navCtrl.navigateForward(['/login']);
  }
  
  ionViewWillEnter() {
     this.getToken();
  }

  getToken() {
    const token = localStorage.getItem('token');
    this.user = this.jwtService.parseJwt(token);
    this.resumenAsesores();
  }

  resumenAsesores() {
    this.service.get(`${this.url}${this.user.id}`)
      .subscribe( resp =>  {
        console.log( resp['response'] );
        this.resul = resp['response']['resumen'];
        this.asesores = resp['response']['sales'];
        console.log( this.asesores );
      }, (err) => {
        console.log( err );
      })
  }


  viewAsesor(id: number) {

    //onsole.log(id);

    let NavigationExtras: NavigationExtras = {
      queryParams: {
        idAsesor: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['/asesor'], NavigationExtras);

  }



}
