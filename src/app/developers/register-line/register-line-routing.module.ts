import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterLinePage } from './register-line.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterLinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterLinePageRoutingModule {}
