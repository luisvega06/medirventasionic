import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { LineModel } from '../../models/line.model';
import { JwtService } from '../../service/jwt.service';
import { HttpService } from '../../service/http.service';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-register-line',
  templateUrl: './register-line.page.html',
  styleUrls: ['./register-line.page.scss'],
})
export class RegisterLinePage implements OnInit {

  lineType: any = [];

  toggleTactico = false;
  togglePorta= false;

  operators: any = [];

  user: any;
  lines;
  formData: LineModel = new LineModel()

  constructor(
    private service: HttpService,
    private jwtService: JwtService,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.user = this.jwtService.parseJwt(token);
    }
    this.getlines()
    this.getLineTypes()
    this.getOperators()
  }


  save() {
  
    this.toggleTactico ? this.formData.tactico = 'SI': this.formData.tactico = 'NO'; 
    this.togglePorta ? this.formData.portability = 'SI': this.formData.portability = 'NO'; 
    
    this.formData['user_id'] = this.user.id;
    Swal.fire({
      icon: 'info',
      allowOutsideClick: false,
      text: 'espere por favor',

    });
    Swal.showLoading();
    this.service.get(`${this.service.urlBase}developers/point/${this.formData.point_code}`)
      .subscribe(resp => {
        this.service.post(`${this.service.urlBase}developers/line_movil`, this.formData)
          .subscribe(resp => {
            this.formData = new LineModel;
            this.toggleTactico = false;
            this.togglePorta = false;
            Swal.fire({
              icon: 'success',
              allowOutsideClick: true,
              text: 'Guardado con exito',
            });
            this.getlines()
          }, response => {

            Swal.fire({
              icon: 'error',
              allowOutsideClick: true,
              text: response.error.error
            });
          })
      }, response => {
        Swal.fire({
          icon: 'success',
          allowOutsideClick: true,
          text: 'ID PDV no se encuenta en la base',
        });
      });
  }

  getlines() {
    this.service.get(`${this.service.urlBase}developers/lines/${this.user.id}`)
      .subscribe(resp => {
        this.lines = resp;
      })
  }

  destoy(id) {
    this.service.post(`${this.service.urlBase}developers/lines/${id}`, {})
      .subscribe(resp => {
        this.getlines()
      })
  }

  salir() {
    localStorage.removeItem('token');
    this.navCtrl.navigateForward(['/login']);
  }

  getLineTypes() {
    this.service.get(`${this.service.urlBase}line_types`)
      .subscribe(resp => {
        Object.values(resp).forEach(type => {
          this.lineType.push(type.name)
        });
      })
  }
  getOperators() {
    this.service.get(`${this.service.urlBase}operators`)
      .subscribe(resp => {
        Object.values(resp).forEach(operator => {
          this.operators.push(operator.name)
        });
      })
  }

  validateCodePoint() {


  }


}
