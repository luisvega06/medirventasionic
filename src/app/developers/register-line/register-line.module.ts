import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterLinePageRoutingModule } from './register-line-routing.module';

import { RegisterLinePage } from './register-line.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterLinePageRoutingModule
  ],
  declarations: [RegisterLinePage]
})
export class RegisterLinePageModule {}
