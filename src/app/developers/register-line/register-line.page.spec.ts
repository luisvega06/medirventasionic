import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterLinePage } from './register-line.page';

describe('RegisterLinePage', () => {
  let component: RegisterLinePage;
  let fixture: ComponentFixture<RegisterLinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterLinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterLinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
