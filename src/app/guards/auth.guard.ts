import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtService } from '../service/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  validation = {
    'asesor pda' : 'venta',
    'asesor pdv': 'venta',
    'asesor mixto': 'venta',
    'supervisor dealer': 'supervisor',
    'asesor': 'venta',
    'desarrollador': 'register-line',
    'gerente dealer': 'gerente',
    'gerente':'gerente',
    'supervisor colombia movil': 'gerente',
    'ejecutivo': 'gerente',
    '*': 'any',
  }
  constructor(private jwtService : JwtService, private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      const token = localStorage.getItem('token');
      const user = this.jwtService.parseJwt(token);
      const roles : string = next.data.rol;

      console.log('guards: ', next);
      

      if(!token){
          this.router.navigate(['/login']);
          return false;
      }
      const userRol = user.rol.toLocaleLowerCase().trim();

      console.log('guards2: ', userRol);
      

      if(roles === '*'){
        return true;
      }

      console.log('include: ', roles.includes(userRol));
      
      if(!roles.includes(userRol)){
        if (this.validation[userRol]) {
          this.router.navigate([this.validation[userRol]]);
          return false;
        } else {
          this.router.navigate(['register-line']);
          return false;
        }
        
      } 
      
      // console.log('GUARD: ', userRol,' -- ', roles);
   
      return true;
  }
  
}
