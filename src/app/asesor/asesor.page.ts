import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { HttpService } from '../service/http.service';


@Component({
  selector: 'app-asesor',
  templateUrl: './asesor.page.html',
  styleUrls: ['./asesor.page.scss'],
})
export class AsesorPage implements OnInit {

  resul:any = [];
  points:any = [];
  id: number;
  private url = this.service.urlBase + 'dealer/ventas/';

  validacion = {
    puntos: 0
  }


  constructor( public navCtrl: NavController,
               private route: ActivatedRoute,
               private service: HttpService ) { }

  ngOnInit() {
    this.getId();
  }

  getId() {
    this.route.queryParams.subscribe( params  =>{
      try{
        this.id = JSON.parse(params['idAsesor']);
        console.log(this.id);
        this.getAsesor();
      }catch(ex){
        console.log(ex);
      }
    })
  }

  getUbications(id: number) {
    console.log('ID del asesor  ', id );

    let NavigationExtras: NavigationExtras = {
      queryParams: {
        id: JSON.stringify(id)
      }
    };
    this.navCtrl.navigateForward(['/mapa'], NavigationExtras);
  }

  validar( dato ) {
    if(dato == 'n')
      this.validacion.puntos = 0

    if(dato == 'a')
      this.validacion.puntos = 1
  }

  getAsesor() {
    this.service.get(`${this.url}${this.id}`)
      .subscribe( resp =>{
        console.log(resp);
        this.resul = resp;
        this.points = resp['response']['visitsFinal'];
        //console.log('total puntos ll', this.points);

      }, (err) => {
        console.log(err);
      })

  }





}
