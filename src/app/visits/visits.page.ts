import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.page.html',
  styleUrls: ['./visits.page.scss'],
})
export class VisitsPage implements OnInit {

  fecha: number = new Date().getTime();

  constructor() { }

  ngOnInit() {
    console.log('mi fecha actual', this.fecha);
    
  }

}
